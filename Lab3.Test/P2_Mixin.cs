﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;

using PK.Test;

namespace Lab3.Test
{
    [TestFixture]
    [TestClass]
    public class P2_Mixin
    {
        [Test]
        [TestMethod]
        public void P2__MixinFor_Should_Be_A_Component_Or_Interface()
        {
            // Assert
            Assert.That(LabDescriptor.MixinFor, Is.EqualTo(LabDescriptor.Component).
                                                Or.EqualTo(LabDescriptor.I1).
                                                Or.EqualTo(LabDescriptor.I2).
                                                Or.EqualTo(LabDescriptor.I3));
        }

        [Test]
        [TestMethod]
        public void P2__Mixin_Should_Be_A_Mixin_For_MixinFor()
        {
            Helpers.Should_Have_An_Extension_Method_For(LabDescriptor.Mixin, LabDescriptor.MixinFor);
        }
    }
}
