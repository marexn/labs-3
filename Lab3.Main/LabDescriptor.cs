﻿using System;

namespace Lab3
{
    public struct LabDescriptor
    {
        #region P1

        public static Type I1 = typeof(IPamiec);
        public static Type I2 = typeof(IMagistrala);
        public static Type I3 = typeof(IProcesor);

        public static Type Component = typeof(UkladSterujacy);

        public delegate object GetInstance(object component);

        public static GetInstance GetInstanceOfI1 = Component => Component;
        public static GetInstance GetInstanceOfI2 = Component => Component;
        public static GetInstance GetInstanceOfI3 = Component => Component;
        
        #endregion

        #region P2

        public static Type Mixin = typeof(MagistralaMixin);
        public static Type MixinFor = typeof(IMagistrala);

        #endregion
    }
}
